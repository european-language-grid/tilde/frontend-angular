import { NgModule } from '@angular/core';

import { MaterialElevationDirective } from './elevate.directive';

@NgModule({
  declarations: [MaterialElevationDirective],
  exports: [MaterialElevationDirective]
})
export class DirectivesModule {}
