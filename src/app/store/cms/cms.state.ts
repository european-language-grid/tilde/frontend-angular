export interface CmsState {
  serverData: any;
  newsData: any;
  savedPageSlug : string;
  catalogueStats: CatalogueStats | null;
  loading: boolean;
}

export interface CatalogueStats {
    corpus: number;
    toolsService: number;
    conceptual: number;
    modelsGrammar: number;
    organization: number;
    projects: number;
}
