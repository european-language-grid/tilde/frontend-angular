import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { CmsMenuService } from '../../+cms/cms-menu.service';
import { environment } from '../../../environments/environment';

import { MenuEffects } from './menu.effects';
import * as fromMenu from './menu.reducer';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature('menu', fromMenu.MenuReducer, {}), EffectsModule.forFeature([MenuEffects])],
  providers: [CmsMenuService]
})
export class MenuModule {}
