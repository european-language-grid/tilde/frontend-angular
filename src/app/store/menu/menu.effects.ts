import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { TransferStateService } from '../../transfer-state.service';
import { flow, get, isEmpty, isNil, negate } from 'lodash/fp';
import { Observable, of as observableOf } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CmsMenuService } from '../../+cms/cms-menu.service';

import { CmsMenuTreeItem, CmsMenuTree } from '../../library/menu/menu.model';

import {
  CurrentRouteSet,
  GetFeedbackMenu,
  GetFeedbackMenuSuccess,
  GetFooterMenu,
  GetFooterMenuSuccess,
  GetMenu,
  GetMenuError,
  GetMenuSuccess,
  MenuTypes
} from './menu.actions';

// import { SearchService } from './search.service';

@Injectable()
export class MenuEffects {
  @Effect() getJSON$: Observable<Action> = this.actions$.pipe(
    ofType(MenuTypes.GET_MENU),
    map(get('payload')),
    switchMap(payload => {
      const url = `${this.config.getSettings('system.cms_root_url')}/api/menu_items/${payload}?_format=hal_json`;

      return  this.transferStateService.fetch(payload, this.http.get<Array<CmsMenuTreeItem>>(url)).pipe(
        map(jsonRet => {
          const newTreeItems: Array<CmsMenuTreeItem> = CmsMenuService.getMenuTree(jsonRet);
          const newTree: CmsMenuTree = {
            menuItems: newTreeItems
          };

          return new GetMenuSuccess(newTree);
        })
      );


    })
  );

  @Effect() gotMenu$: Observable<Action> = this.actions$.pipe(
    ofType(MenuTypes.SET_CURRENT_ROUTE),
    map(get('payload')),
    // tslint:disable-next-line:arrow-return-shorthand
    switchMap(() => observableOf(new CurrentRouteSet())
    )
  );

  @Effect() getFooterMenu$: Observable<Action> = this.actions$.pipe(
    ofType(MenuTypes.GET_FOOTER_MENU),
    map(get('payload')),
    switchMap(payload => {
      const url = `${this.config.getSettings('system.cms_root_url')}/api/menu_items/${payload}?_format=hal_json`;
      // console.log('url: ', url);

      return this.transferStateService.fetch(payload, this.http.get<Array<CmsMenuTreeItem>>(url)).pipe(
        map(jsonRet => {
          const newTreeItems: Array<CmsMenuTreeItem> = CmsMenuService.getMenuTree(jsonRet);
          const newTree: CmsMenuTree = {
            menuItems: newTreeItems
          };

          return new GetFooterMenuSuccess(newTree);
        })
      );
    })
  );

  @Effect() getFeedbackMenu$: Observable<Action> = this.actions$.pipe(
    ofType(MenuTypes.GET_FEEDBACK_MENU),
    map(get('payload')),
    switchMap(payload => {
      const url = `${this.config.getSettings('system.cms_root_url')}/api/menu_items/${payload}?_format=hal_json`;

      return this.transferStateService.fetch(payload, this.http.get<Array<CmsMenuTreeItem>>(url)).pipe(
        map(jsonRet => {
          const newTreeItems: Array<CmsMenuTreeItem> = CmsMenuService.getMenuTree(jsonRet);
          const newTree: CmsMenuTree = {
            menuItems: newTreeItems
          };

          return new GetFeedbackMenuSuccess(newTree);
        })
      );
    })
  );

  constructor(
    private readonly actions$: Actions,
    private readonly http: HttpClient,
    private readonly config: ConfigService,
    private readonly cmsMenuService: CmsMenuService,
    private readonly transferStateService: TransferStateService
  ) {}
}
