import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { flow, get, isEmpty, isNil, negate } from 'lodash/fp';
import { Observable, of as observableOf } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';

import { WebformElements, WebFormT } from '../../library/webforms/webforms.model';

import { GetWebformElementsSuccess, GetWebformFieldsSuccess, WebformActionTypes, PostWebformSuccess, PostWebformError } from './webform.actions';

// import { SearchService } from './search.service';

const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class WebformEffects {
  skipjWT = new HttpHeaders().set(InterceptorSkipHeader, '');

  @Effect() getWebform$: Observable<Action> = this.actions$.pipe(
    ofType(WebformActionTypes.GET_WEBFORM_FIELDS),
    map(get('payload')),
    switchMap(payload => {
      const url = `${this.config.getSettings('system.cms_root_url')}/webform_rest/${payload}/fields?_format=hal_json`;

      return this.http.get<WebFormT>(url).pipe(map(jsonRet => new GetWebformFieldsSuccess(jsonRet)));
    })
  );

  @Effect() getWebformElems$: Observable<Action> = this.actions$.pipe(
    ofType(WebformActionTypes.GET_WEBFORM_ELEMENTS),
    map(get('payload')),
    switchMap(payload => {
      const url = `${this.config.getSettings('system.cms_root_url')}/webform_rest/${payload}/elements?_format=hal_json`;

      return this.http.get<WebformElements>(url).pipe(map(jsonRet => new GetWebformElementsSuccess(jsonRet)));
    })
  );

  @Effect() postWebform$: Observable<Action> = this.actions$.pipe(
    ofType(WebformActionTypes.POST_WEBFORM),
    map(get('payload')),
    switchMap(payload => {
      const url = `${this.config.getSettings('system.cms_root_url')}/webform_rest/submit?_format=hal_json`;

      return this.http.post<any>(url, payload, { headers: this.skipjWT }).pipe(map(jsonRet => {
        if (jsonRet.error) {
          return new PostWebformError(jsonRet);
        } else {
          return new PostWebformSuccess(jsonRet);
        }
      }));
    })
  );

  // @Effect() gotMenu$: Observable<Action> = this.actions$.pipe(
  //   ofType(MenuTypes.SET_CURRENT_ROUTE),
  //   map(get('payload')),
  //   // tslint:disable-next-line:arrow-return-shorthand
  //   switchMap(() => {
  //     return observableOf(new CurrentRouteSet());
  //   })
  // );

  constructor(private readonly actions$: Actions, private readonly http: HttpClient, private readonly config: ConfigService) {}
}
