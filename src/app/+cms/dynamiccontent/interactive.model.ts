export interface InteractiveData {
  type: string; // elg-link, primary-button etc
  value: string | null; // everything whats between tags ex. value text, img
  classList: DOMTokenList | null; // all the classes that element posesses
  link: string | null; // href link if element has one
  src: string | null; // src if element has one
  src_compressed: string | null; // src_compressed if element has one
  alt: string | null; // alt link if element has one
  styling: CSSStyleDeclaration | null; // extra styling, coming from customized elements
  genericData: string | null; // any additional info that one might want to pass through
}

