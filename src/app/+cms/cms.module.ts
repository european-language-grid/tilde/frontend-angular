import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CmsComponent } from './cms.component';
import { CmsMenuService } from './cms-menu.service';
import { CmsService } from './cms.service';
import { CommonModule } from '@angular/common';
import { DomService } from "./dom.service";
import { DynamicModule } from './dynamiccontent/dynamic.module';
import { DynamicService } from './dynamiccontent/dynamic.service';
import { EscapeHtmlPipe } from './keep-html.pipe';
import { MaterialModule } from '../framework/material';
import { NgModule } from '@angular/core';
import { PageComponent } from './page/page.component';
import { RouterModule } from '@angular/router';
import { SearchModule } from '../search/search.module';
import { SharedModule } from '../framework/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    MaterialModule,
    DynamicModule,
    SearchModule,
  ],
  providers: [
    CmsService,
    CmsMenuService,
    DomService,
    DynamicService
  ],
  declarations: [
    CmsComponent,
    PageComponent,
    EscapeHtmlPipe
  ]
})
export class CmsModule {}
