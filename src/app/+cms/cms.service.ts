import { ConfigService } from '@ngx-config/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TransferStateService } from '../transfer-state.service';

export class CmsPageContent {
    type: string;
    title: string;
    subtitle: string;
    created: Date;
    changed: Date;
    body:  Array<string> = [];
    loading: boolean;
    error:  boolean;
    show_search_field: boolean;
    author: string;
    view_node: string;

    constructor(){
        this.type = '';
        this.title = '';
        this.subtitle = '';
        this.loading = true;
        this.error = false;
        this.show_search_field = false;
    }

    loadString(_cmsContent: _CmsPageContent, cmsUrl: string, cmsPath: string, baseUrl: string):void {
        this.body = [];

        if (_cmsContent !== undefined) {
            if (_cmsContent._links !== undefined && _cmsContent._links.type !== undefined) {
                this.type = _cmsContent._links.type.href.includes("article") ? "article" : "page";
            }
            this.title = _cmsContent.title.length > 0 ? _cmsContent.title[0].value : '' as string;
            if (!this.title) {
                this.title = _cmsContent.title as undefined as string;
            }
            if (_cmsContent.body !== undefined) {
                this.body.push(this.clean(_cmsContent.body[0].value ? _cmsContent.body[0].value : (_cmsContent.body as unknown) as string, cmsUrl, baseUrl));
            }
            if (_cmsContent.field_section !== undefined) {
                for (const html of _cmsContent.field_section){
                    this.body.push(this.clean(html.value, cmsUrl, baseUrl));
                }
            }
            // tslint:disable-next-line:prefer-conditional-expression
            if (_cmsContent.field_description !== undefined) {
                this.subtitle = _cmsContent.field_description.length > 0 ? _cmsContent.field_description[0].value : '';
            } else {
                this.subtitle = '';
            }

            if (_cmsContent.uid !== undefined) {
                this.author = _cmsContent.uid.length > 0 ? _cmsContent.uid : '';
            } else {
                this.author = '';
            }
            if (_cmsContent.view_node !== undefined) {
                this.view_node = _cmsContent.view_node.length > 0 ? _cmsContent.view_node : '';
            } else {
                this.author = '';
            }

            _cmsContent.field_display_search && _cmsContent.field_display_search[0].value ? this.show_search_field = true : this.show_search_field = false;


            let createdDate = _cmsContent.created.length > 0 ? _cmsContent.created[0].value : 0;

            if (!createdDate) {
                createdDate = _cmsContent.created as unknown as number;
            }

            this.created = new Date(createdDate);


            this.changed = new Date(_cmsContent.changed.length > 0 ? _cmsContent.changed[0].value : 0);
            // let links = JSON.parse(JSON.stringify(_cmsContent._links));
        }
    }

    clean(bo: string, cmsPath: string, baseUrl: string): string {
        let cleanString: string = bo;
        // lai bildes hostotas drupalā iegūtu pareizu ceļu
        // tslint:disable-next-line:prefer-template
        cleanString = cleanString.replace(/( *src=")(?!http:\/\/)(?!https:\/\/)(.*?)"/g, '$1' + cmsPath + '$2"');
        // download linki
        // tslint:disable-next-line:prefer-template
        cleanString = cleanString.replace(/( *href=")(\/sites\/default\/files\/download\/)(.*?)"/g, '$1' + this.removeLastChar(cmsPath, '/') + '$2$3"');
        // ja download linki sākas ar /downloads, tad tos jāservē no frontenda
        // tslint:disable-next-line:prefer-template
        cleanString = cleanString.replace(/( *href=")(\/downloads\/)(.*?)"/g, '$1' + this.removeLastChar(baseUrl, '/') + '$2$3"');
        // linki, kuri ved uz portāla lapu, lai pārtop par angular linkiem
        cleanString = cleanString.replace(/( *href=")(\/)(.*?)"/g, ' data-link="/$3"');
        // visi ārējie linki, lai verās jaunā tabā
        cleanString = cleanString.replace(/( *href=")(?!https:\/\/ | !http:\/\/)(?!#)(.*?)"/g, '$1$2" target="_blank"');
        // linki, kuriem jāverās jaunā tabā, bet nesākās ar ftp|http|https|mailto|#
        cleanString = cleanString.replace(/( *href=")(?!ftp:\/\/)(?!http:\/\/)(?!https:\/\/)(?!mailto:)(?!#)(?!\/)(.*?)"/g, '$1http://$2" target="_blank"');
        return cleanString;
    }

    removeLastChar(str: string, char: string):string {
        if(str !== undefined && str.length > 0){
            const n = str.lastIndexOf(char);

            return str.substring(0, n);
        }
    }
}

export class CmsNewsContent {
    news: Array<_CmsNewsContent> = [];
    loading: boolean;
    error: boolean;

    constructor(){
        this.loading = true;
        this.error = false;
    }
}


export interface Pager {
    "current_page": number,
    "items_per_page": number,
    "total_items": string,
    "total_pages": number
}

@Injectable()
export class CmsService {

    cmsPageContent: CmsPageContent;
    cmsNewsContent: CmsNewsContent;

    constructor(
        private readonly http: HttpClient,
        private readonly config: ConfigService,
        private readonly transferStateService: TransferStateService
    ) {
        this.cmsPageContent = new CmsPageContent();
    }

    getCmsPageContent(alias: string): CmsPageContent {
        if (this.config.getSettings('system.cms_base_url') === undefined || this.config.getSettings('system.cms_base_url') === "") {
            // console.log('configuration missing');
            return new CmsPageContent();
        }

        this.cmsPageContent = new CmsPageContent();
        this.cmsPageContent.loading = true;
        this.cmsPageContent.error = false;

        this.subCmsPageContent(alias).subscribe(
            result => {
                this.cmsPageContent.loadString(
                    result,
                    this.config.getSettings('system.cms_base_url'),
                    this.config.getSettings('system.cms_root_url'),
                    this.config.getSettings('system.applicationUrl')
                );

                this.cmsPageContent.loading = false;
            },
            error => {
                this.cmsPageContent.error = true;
                this.cmsPageContent.loading = false;
                console.error(error);
            }
        );
        return this.cmsPageContent;
    }

    getCmsNewsContent(alias: string): CmsNewsContent {
        console.log("getCmsNewsContent", alias);
        if (this.config.getSettings('system.cms_base_url') === undefined || this.config.getSettings('system.cms_base_url') === "") {
            return new CmsNewsContent();
        }

        this.cmsNewsContent = new CmsNewsContent();
        this.cmsNewsContent.loading = true;
        this.cmsNewsContent.error = false;

        this.http.get<Array<_CmsNewsContent>>(`${this.config.getSettings('system.cms_root_url')}/${alias}?_format=hal_json`).subscribe(
            result => {
                this.cmsNewsContent.news = result;
                this.cmsNewsContent.loading = false;
            },
            error => {
                this.cmsNewsContent.loading = false;
                this.cmsNewsContent.error = true;
                console.error(error);
            }
        );

        return this.cmsNewsContent;
    }

    subCmsPageContent(alias: string, pagenumber?: string): Observable<_CmsPageContent> {
        console.log("subCmsPageContent", alias);
        const pageSlug_clean = alias.substring(0,1) === '/' ? alias.substring(1, alias.length) : alias;
        const cms_root_url = this.config.getSettings('system.cms_root_url');
        const cms_root_url_clean = cms_root_url.substring(cms_root_url.length-1,1) === '/' ? cms_root_url.substring(0, cms_root_url.length-1) : cms_root_url;
        const pgnum = pagenumber ? `&page=${pagenumber}` : '';

       return this.transferStateService.fetch(pageSlug_clean, this.http.get<_CmsPageContent>(`${cms_root_url_clean}/${pageSlug_clean}?_format=hal_json${pgnum}`))

    }

    getCatalogueStatistics(catalogueUrl: string): Observable<any> {

        return  this.http.get<_CmsPageContent>(`${catalogueUrl}/registry/search/`);
    }
}



// tslint:disable-next-line:class-name
interface _CmsPageContent {
    title: Array<_CmsPageContentTitle>;
    created: Array<_CmsPageContentDateValue>;
    changed: Array<_CmsPageContentDateValue>;
    body: Array<_CmsPageContentBody>;
    field_section: Array<_CmsPageContentBody>;
    field_description: Array<any>;
    _links: Links;
    webform: Array<Webform>;
    field_display_search: Array<KeyValue>;
    uid: string;
    view_node: string;
}

interface Links {
    type: Href;
}

interface Href {
    href: string;
}

interface KeyValue {
    value: boolean;
}

interface Webform {
    close: string;
    default_data: string;
    open: string;
    status: string;
    target_id: string;
}

// tslint:disable-next-line:class-name
interface _CmsPageContentTitle {
    value: string;
}
// tslint:disable-next-line:class-name
interface _CmsPageContentDateValue {
    value: number;
}
// tslint:disable-next-line:class-name
interface _CmsPageContentBody {
    value: string;
    format: string;
    summary: string;
}


// tslint:disable-next-line:class-name
interface _CmsNewsContent {
    created: string;
    title: string;
    path: string;
    body: string;
}
// tslint:disable-next-line:class-name
interface _Statistics {
    entryCount: number;
    termCount: number;
    collectionCount: number;
    languageCount: number;
}