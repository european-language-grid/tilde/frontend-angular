import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';

import { CmsMenuTree, CmsMenuTreeItem } from '../library/menu/menu.model';



@Injectable()
export class CmsMenuService {

    constructor(
        private readonly http: HttpClient,
        private readonly config: ConfigService,
    ) { }

    static parsePath(path: string, external: boolean, angularLink: boolean) : string | null {
        // if home - return /
        if (path === "/cms/"){
            return "/";
        }
        // external === true - add nothing
        if (external ){
            return path;
        }

        // angularLink === true - add nothing
        if (angularLink){
            return path.replace('/cms/', '/');
        }

        // else - add page
        // tslint:disable-next-line:prefer-template
        return `${path.replace('/cms/', '/')}`;
    }

    static getMenuType(external: boolean, angular: string):string {
        return (external ? "external" : (angular === '' ? "angular" : "page" ));
    }

    static getMenuTree(items: Array<CmsMenuTreeItem>): Array<CmsMenuTreeItem> {

        return items;
    }

    // static getItems(items: Array<any>): Array<CmsMenuTreeItem> {
    //     if (items === undefined || items.length === 0) {return [];}

    //     const parsed: Array<CmsMenuTreeItem> = [];
    //     // nomainām ceļus
    //     items.forEach((i1:CmsMenuTreeItem) => {
    //         const parsedItem: CmsMenuTreeItem = { ...i1 };
    //         parsedItem.link.url = CmsMenuService.parsePath(i1.link.url, parsedItem.link.options.external, (i1.link.route_name === ''));
    //         parsedItem.type = CmsMenuService.getMenuType(parsedItem.link.options.external, i1.link.route_name);

    //         if (i1.has_children) {
    //             parsedItem.below = [];

    //             i1.subtree.forEach((i2:CmsMenuTreeItem) => {
    //                 i2.uri = CmsMenuService.parsePath(i2.link.url, i2.link.options.external, (i2.link.route_name === ''));
    //                 i2.type = CmsMenuService.getMenuType(i2.link.options.external, i2.link.route_name);
    //                 i2.breadcrumbs = [{
    //                     title: parsedItem.link.title,
    //                     path: parsedItem.link.url,
    //                     depth: parsedItem.depth},
    //                     {
    //                     title: i2.link.title,
    //                     path: i2.uri,
    //                     depth: i2.depth}];

    //                 if (i2.has_children) {
    //                     i2.subtree.forEach(i3 => {
    //                         i3.uri = CmsMenuService.parsePath(i3.link.url, i3.link.options.external, (i3.link.route_name === ''));
    //                         i3.type = CmsMenuService.getMenuType(i3.link.options.external , i3.link.route_name);
    //                         i3.breadcrumbs = [{
    //                             title: parsedItem.link.title,
    //                             path: parsedItem.link.url,
    //                             depth: parsedItem.depth},
    //                             {
    //                             title: i2.link.title,
    //                             path: i2.uri,
    //                             depth: i2.depth},
    //                             {
    //                                 title: i3.link.title,
    //                                 path: i3.uri,
    //                                 depth: i3.depth}];

    //                     });
    //                 }

    //                 parsedItem.below.push(i2);
    //             });
    //         }
    //         parsed.push(parsedItem);

    //     });

    //     return parsed;
    // }

    getMenu(name: string):Observable<Array<CmsMenuTree>> {
        return this.http.get<Array<CmsMenuTree>>(`${this.config.getSettings('system.cms_root_url')}/api/menu_items/${name}?_format=hal_json`)
    }
}

