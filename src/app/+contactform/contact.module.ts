import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../framework/core';
import { MaterialModule } from '../framework/material';
import { PipesModule } from '../shared/pipes/pipes.module';

import { ContactComponent } from './contact.component';
import { routes } from './contact.routes';





@NgModule({
  imports: [
    CommonModule, 
    FormsModule, 
    ReactiveFormsModule, 
    RouterModule.forChild(routes), 
    SharedModule, 
    MaterialModule,
    PipesModule
  ],
  declarations: [
    ContactComponent
  ],

})
export class ContactModule {}
