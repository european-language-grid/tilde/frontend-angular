import { NewsComponent } from './news.component';

export const routes = [
  {
    path: '',
    component: NewsComponent,
    data: {
      meta: {
        title: 'Showdown tilte',
        description: 'Showdown desc'
      }
    }
  },
  {
    path: ':pagenumber',
    component: NewsComponent,
    data: {
      meta: {
        title: 'Showdown tilte',
        description: 'Showdown desc'
      }
    }
  }
];
