import { ChangeDetectorRef, Component, Inject, Injector, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from "@angular/common";
import { KeycloakService } from 'keycloak-angular';
import { BaseComponent } from '../framework/core';
import { routeAnimation } from '../shared';
import { CookieService } from '../layout/foot/cookie.service';
import { ProviderApplicantService } from '../request-provider/provider-applicant.service';
import { ConfigService } from '@ngx-config/core';
import { Store } from '@ngrx/store';
import { State } from '../store/state';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { AuthState } from '../store/auth/auth.state'


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [routeAnimation],
})
export class ProfileComponent extends BaseComponent implements OnInit {
  userDetails: any;
  tok: any;
  idToken: any;
  isAuthenticated = false;
  isBrowser = isPlatformBrowser(this.platformId);
  memberSince: Date;
  secondsSinceRegistered: number;
  displayWelcomeBanner = false;

  changePasswordUrl = "";
  updateProfileUrl = "";
  applicationsUrl = "";


  // eslint-disable-next-line @typescript-eslint/unbound-method
  parseUrl = ProfileComponent.validURL;   // to call static method from template

  constructor(
    private readonly cd: ChangeDetectorRef,
    private readonly keycloakService: KeycloakService,
    @Inject(PLATFORM_ID) private readonly platformId: any,
    private readonly providerApplicant: ProviderApplicantService,
    private readonly cookieService: CookieService,
    private readonly config: ConfigService,
    private readonly appStore: Store<State>,
  ) {
    super();
   }



   static parseJwt(tk: string): any {
    if (tk !== undefined && tk.split(".").length < 3) {
      return JSON.parse("{}");
    }

    const base64Url = tk.split(".")[1];
    const base64 = base64Url.replace("-", "+").replace("_", "/");

    return JSON.parse(atob(base64));
  }

  static validURL(str:string):boolean {
    const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

    return !!pattern.test(str);
  }

  async ngOnInit(): Promise<void> {
    this.appStore.select((st: State) => st.auth).pipe(
      map(fields => ((fields as unknown) as AuthState)),
      distinctUntilChanged()
    ).subscribe((_state) => {
      this.isAuthenticated = _state.isAuthenticated;

      if (this.isAuthenticated) {
        this.isAuthenticated = true;
        this.userDetails = _state.idToken;
        this.secondsSinceRegistered = Math.round(this.userDetails.sec_since_reg);
        if(this.cookieService.getCookie("display_welcome_banner") !== "false" && this.secondsSinceRegistered < 300){
          this.displayWelcomeBanner = true;
        }

        this.changePasswordUrl = `${this.config.getSettings('system.keycloak_url')}/realms/ELG/account/password?referrer=angular-frontend&referrerUrl=${this.config.getSettings('system.cms_base_url')}/`;
        this.updateProfileUrl = `${this.config.getSettings('system.keycloak_url')}/realms/ELG/account/?referrer=angular-frontend&referrerUrl=${this.config.getSettings('system.cms_base_url')}/`;
        this.applicationsUrl = `${this.config.getSettings('system.keycloak_url')}/realms/ELG/account/applications?referrer=angular-frontend&referrerUrl=${this.config.getSettings('system.cms_base_url')}/`;

        this.cd.markForCheck();
      }
    });

  }

  async logout(): Promise<void> {
    if (this.isBrowser) {
      return this.keycloakService.logout();
    }
  }

  hideBanner($event): void {
    $event.preventDefault();
    this.displayWelcomeBanner = false;
    this.cookieService.setCookie("display_welcome_banner", 'false', 365);
  }

}
