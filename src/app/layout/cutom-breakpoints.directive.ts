import {Directive} from '@angular/core';
import {LayoutDirective} from '@angular/flex-layout';

const selector = `[fxFlex.sd]`;
const inputs = ['fxFlex.sd'];

@Directive({selector, inputs})
export class CustomShowHideDirective extends LayoutDirective {
  protected inputs = inputs;
}