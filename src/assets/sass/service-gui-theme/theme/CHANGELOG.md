# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [3.1.0](https://github.com/material-components/material-components-web/compare/v3.0.0...v3.1.0) (2019-07-22)

### Features

- **data-table:** Added data table component ([#4889](https://github.com/material-components/material-components-web/issues/4889)) ([7d3380a](https://github.com/material-components/material-components-web/commit/7d3380a))

# [3.1.0-alpha.0](https://github.com/material-components/material-components-web/compare/v3.0.0...v3.1.0-alpha.0) (2019-07-16)

### Features

- **data-table:** Added data table component ([#4889](https://github.com/material-components/material-components-web/issues/4889)) ([7d3380a](https://github.com/material-components/material-components-web/commit/7d3380a))

# [3.1.0](https://github.com/material-components/material-components-web/compare/v3.0.0...v3.1.0) (2019-07-16)

### Features

- **data-table:** Added data table component ([#4889](https://github.com/material-components/material-components-web/issues/4889)) ([7d3380a](https://github.com/material-components/material-components-web/commit/7d3380a))
